<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="roompage.css" />
    <title>JML Hotel</title>
</head>
<body>
<div id="header"></div>
<a href="Homepage.php"><img id="logoimg" src="images/JMLLogo.png"></a>
<div id="contactdetails">
    J.P. Laurel Avenue, Bo. Obrero, Davao City<br>
    (87)-012-3467<br>
    <a id="gmaillink" href="https://www.gmail.com.ph">JMLHotel@gmail.com<br></a>
</div>
</body>
</html>