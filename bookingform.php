<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="stylebookingform.css" />
    <title>Sign up</title>
</head>
<body>
 <div id="both">
        <div id="customerinfo">
                <form>
                    <h3>Customer Information</h3>
                    First Name: <input type="text" name="fname"><br>
                    Last Name: <input type="text" name="lname"><br>
                    Sex: <input type="radio" name="gender" value="male">Male
                    <input type="radio" name="gender" value="female">Female<br>
                    Company Name: <input type="text" name="comapany"><br>
                    House No: <input type="text" name="houseno" size="3px">
                    Street <input type="text" name="street" size="10px"><br>
                    Barangay: <input type="text" name="barangay"><br>
                    City: <input type="text" name="city"><br>
                    Country: <input type="text" name="country"><br>
                    Phone: <input type="text" name="phone"><br>
                    Email: <input type="text" name="email"><br>
                    Username: <input type="text" name="username"><br>
                    Password: <input type="password" name="password">
                </form>
            </div>
        
            <div id="paymentinfo">
                <form>
                    <h3>Payment Information</h3>
                    Payment Method: 
                    <select name="paymentmethod">
                        <option value="debitcard">Debit Card</option>
                        <option value="visa">Visa</option>
                        <option value="creditcard">Credit Card</option>
                        <option value="cash">Cash</option>
                    </select><br>
                    Name on Card: <input type="text" name="cardname"><br>
                    Security Code: <input type="password" name="seccode">

                    <h3>Booking Info</h3>
                    Check In:
                    <select name="checkinmonth">
                        <option value="jan">January</option>
                    </select>
                    <select name="checkinday">
                        <option value="1">1</option>
                    </select>
                    <select name="checkinyear">
                        <option value="2018">2018</option>
                    </select><br>

                    Check Out:
                    <select name="checkoutmonth">
                        <option value="jan">January</option>
                    </select>
                    <select name="checkoutday">
                        <option value="1">1</option>
                    </select>
                    <select name="checkoutyear">
                        <option value="2018">2018</option>
                    </select><br>

                    Car:
                    <select name="car">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>

                    <br><br><br>
                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>