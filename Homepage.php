<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <title>JML Hotel</title>
</head>
<body>
    <div id="header"></div>
    <a href="Homepage.php"><img id="logoimg" src="images/JMLLogo.png"></a>
    <div id="contactdetails">
        J.P. Laurel Avenue, Bo. Obrero, Davao City<br>
        (87)-012-3467<br>
        <a id="gmaillink" href="https://www.gmail.com.ph">JMLHotel@gmail.com<br></a>
    </div>
    <div class="tagline">
        <h2>"Enjoy the Warm and Cozy Hotel at the heart of the City!"</h2>
    </div>

    <div id="login">
        <a href="bookingform.php" id="login-link">Sign up</a> or 
        <a href = "login.php" id = "login-link"> Log in here </a>
    </div>

    <div id="footer"></div>
    <p id="rights">All rights reserved 2018</p>

    <div id="accounts">
        <a href="https:www.facebook.com/JMLHotel"><img id="smaccounts" src="images/Facebook.png" alt="Facebook"></a>
        <a href="https:www.twitter.com/JMLHotel"><img id="smaccounts" src="images/Twitter.png" alt="Twitter"></a>
        <a href="https:www.Instagram.com/JMLHotel"><img id="smaccounts" src="images/Instagram.png" alt="Instagram"></a>
        <a href="https:www.Pinterest.com/JMLHotel"><img id="smaccounts" src="images/Pinterest.png" alt="Pinterest"></a>
        <a href="https:www.Gmail.com/JMLHotel"><img id="smaccounts" src="images/Gmail.png" alt="Gmail"></a>
    </div>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <input type="submit" name="booknow" value="Book Now!">
    </form>

    <?php
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            if($_POST["booknow"] == "Book Now!"){
                header("location:roompage.php");
            }
        }
    ?>

</body>
</html>